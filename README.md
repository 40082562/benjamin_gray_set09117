# README #

includes instructions on building the app

### What is this repository for? ###

This Repository will contain all the code that I have writen to implement a game of checkers using C++, AI for this project utilises MiniMax algorythm.

### Setting up the space ###
*The project was built using visual studio 2017 toolset platform v141 
*if the toolset version is lower the application will not compile as some elements are named differently.
* if you need to run this in anything lower than V141 please open the "Checkers Legacy soloution" within the source code file.
* older versions will get an error on opening click yes. 
* to run the console app you must build the project with the following in place;
* The version of the project should be "10.0.16299.0" or V141 - unless using the legacy code
* This can be changed in the project properties settings under Config Properties -> General -> Windows SDK version.
* you must be in debug mode and x86
* run the application
