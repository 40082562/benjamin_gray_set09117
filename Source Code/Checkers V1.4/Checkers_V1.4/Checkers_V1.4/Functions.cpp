#include "stdafx.h"
#include "Functions.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <stack>

void PrepGame() {
	//prepare variables:
	selection = " ";
	undoSQSelection = " ";
	undoTargSelection = " ";
	startMoveValue = " ";
	endMoveValue = " ";
	stringSave = " ";
	turn = Red;
	CapDir = ' ';
	initialRowOddEven = ' ';
	wasCaptured = false;
	loser = ' ';
	selected = 0;
	targeted = 0;
	SQbetween = 0;
	redoSelect = 0;
	redoTarget = 0;
	redoCapture = 0;
	undoSelect = 0;
	undoTarget = 0;
	minselectVal = 0;
	maxSelectVal = 32;
	minTargetVal = 0;
	maxTargetVal = 0;
	AITargetMove = 0;


	//prepare squares and pieces:
	squares[0] = sq::Square(Red, "a1", '1'); squares[1] = sq::Square(Red, "c1", '1');
	squares[2] = sq::Square(Red, "e1", '1'); squares[3] = sq::Square(Red, "g1", '1');
	squares[4] = sq::Square(Red, "b2", '2'); squares[5] = sq::Square(Red, "d2", '2');
	squares[6] = sq::Square(Red, "f2", '2'); squares[7] = sq::Square(Red, "h2", '2');
	squares[8] = sq::Square(Red, "a3", '3'); squares[9] = sq::Square(Red, "c3", '3');
	squares[10] = sq::Square(Red, "e3", '3'); squares[11] = sq::Square(Red, "g3", '3');
	squares[12] = sq::Square(' ', "b4", '4'); squares[13] = sq::Square(' ', "d4", '4');
	squares[14] = sq::Square(' ', "f4", '4'); squares[15] = sq::Square(' ', "h4", '4');
	squares[16] = sq::Square(' ', "a5", '5'); squares[17] = sq::Square(' ', "c5", '5');
	squares[18] = sq::Square(' ', "e5", '5'); squares[19] = sq::Square(' ', "g5", '5');
	squares[20] = sq::Square(Black, "b6", '6'); squares[21] = sq::Square(Black, "d6", '6');
	squares[22] = sq::Square(Black, "f6", '6'); squares[23] = sq::Square(Black, "h6", '6');
	squares[24] = sq::Square(Black, "a7", '7'); squares[25] = sq::Square(Black, "c7", '7');
	squares[26] = sq::Square(Black, "e7", '7'); squares[27] = sq::Square(Black, "g7", '7');
	squares[28] = sq::Square(Black, "b8", '8'); squares[29] = sq::Square(Black, "d8", '8');
	squares[30] = sq::Square(Black, "f8", '8'); squares[31] = sq::Square(Black, "h8", '8');
	return;
}

bool isSquare(std::string sq) {
	//only used in constructor
	if (sq == "a1" || sq == "c1" || sq == "e1" || sq == "g1"
		|| sq == "b2" || sq == "d2" || sq == "f2" || sq == "h2"
		|| sq == "a3" || sq == "c3" || sq == "e3" || sq == "g3"
		|| sq == "b4" || sq == "d4" || sq == "f4" || sq == "h4"
		|| sq == "a5" || sq == "c5" || sq == "e5" || sq == "g5"
		|| sq == "b6" || sq == "d6" || sq == "f6" || sq == "h6"
		|| sq == "a7" || sq == "c7" || sq == "e7" || sq == "g7"
		|| sq == "b8" || sq == "d8" || sq == "f8" || sq == "h8") return true;
	return false;
}

char setCrown(char color) {
	//returns either the crowned version of a color ('R' or 'B')
	//or uncrowned version ('r' or 'b')
	if (color == Red) return cRed;
	if (color == Black) return cBlack;
	if (color == cRed) return Red;
	if (color == cBlack) return Black;
	error("Exception: setCrown() used on a char that is not 'r', 'b', 'R', or 'B'\n");
}


bool cantMove() {
	for (size_t i = 0; i < squares.size(); ++i)
	{
		if (possibleMove(&squares[i]))
		{
			return false; //a move is possible
		}
		if (posCapture(&squares[i])) return false;
	}
	return true;
}

bool possibleMove(sq::Square *initSq) {
	//can the piece on initSq move one square?

	//if initSq's color is wrong, return false
	if (turn != initSq->color() && setCrown(turn) != initSq->color())
	{
		return false;
	}

	switch (initSq->color()) {
	case Red:
		//if red, can the piece move one square forward
		for (size_t i = 0; i < initSq->getFrontAdjSqs().size(); ++i)
		{
			//size_t i holds the address of an adjacent square
			//check if the square with the same address as the
			//adjacent square is blank (meaning a piece can move
			//there and movement is possible)
			if (initSq->getFrontAdjSqs()[i] != " " && //prevent using fetchAddress on non-square
				squares[fetchAddress(initSq->getFrontAdjSqs()[i])].color() == ' ')
			{
				return true;
			}
		}
		break;
	case Black:
		//if black, can the piece move one square backward (from our view)
		for (size_t i = 0; i < initSq->getBackAdjSqs().size(); ++i)
		{
			if (initSq->getBackAdjSqs()[i] != " " && //prevent using fetchAddress on non-square
				squares[fetchAddress(initSq->getBackAdjSqs()[i])].color() == ' ')
			{
				return true;
			}
		}
		break;
	case cRed: case cBlack:
		//if crowned, can the piece move one square any direction
		for (size_t i = 0; i < initSq->getFrontAdjSqs().size(); ++i) //here, size() returns 2 and works for both
																   //getFrontAdjSqs and getBacAdjSqs
		{
			if (initSq->getFrontAdjSqs()[i] != " " && //prevent using fetchAddress on non-square
				squares[fetchAddress(initSq->getFrontAdjSqs()[i])].color() == ' ')
			{
				return true;
			}
			if (initSq->getBackAdjSqs()[i] != " " && //prevent using fetchAddress on non-square
				squares[fetchAddress(initSq->getBackAdjSqs()[i])].color() == ' ')
			{
				return true;
			}
		}
		break;
	}
	return false;
}

void fetchSquare() {
	std::cout << "Current turn: " << turn << "\n";
	std::cout << "Enter coordinate of piece you want to move (ex. a1, f8):\n";
	std::cin >> selection;
	startMoveValue = selection;
	
	
	if (selection == "u" || selection == "undo")
	{
		undoLastMove();
		fetchSquare();
	}
	if (selection == "r" || selection == "redo")
	{
		redoMove();
		fetchSquare();
	}
	if (selection == "h" || selection == "help")
	{
		ShowHelpMenu(); fetchSquare();
	}
	if (selection == "d" || selection == "display")
	{
		displayBoard(squares, 2); fetchSquare();
	}
	if (selection == "m" || selection == "reset")
	{
		displayBoard(squares, 2); std::cout << "\nCannot reset! No selected piece.\n"; fetchSquare();
	}
	if (selection == "s" || selection == "save")
	{
		saveGame(); fetchSquare();

	}
	if (selection == "q" || selection == "quit") return;
	if (!goodSquare(selection)) fetchSquare();
	// adds the entered value to startMoveValue to be stored in the stack ocnec it is confirmed as a good square
	startMoveValue = selection;
	return;
}

bool goodSquare(std::string sq) {
	//checks if color of piece on the square is correct and
	//if square is accessible

	if (!isSquare(sq)) { std::cout << "\nError: You did not select an accessible square\n"; return false; }

	selected = fetchAddress(selection); //fetchAddress(...) after isSquare to prevent crash
	MovesMade.push(selected);									//(i.e. There's no address for non-squares)

	if (turn != squares[selected].color() && setCrown(turn) != squares[selected].color())
	{
		std::cout << "\nError: Picked wrong color.\n"; return false;
	}
	return true; //Success!
}

void fetchTarget() {
	if (selection == "q" || selection == "quit")
		return;
	std::cout << "Enter coordinate of target square (ex. a1, f8):\n";
	std::cin >> selection;
	endMoveValue = selection;
	//MovesMade.push(endMoveValue);
	if (selection == "u" || selection == "undo")
	{
		undoLastMove();
		fetchSquare();
	}
	if (selection == "r" || selection == "redo")
	{
		redoMove();
		fetchSquare();
	}
	if (selection == "h" || selection == "help")
	{
		ShowHelpMenu(); fetchTarget();
	}
	if (selection == "d" || selection == "display")
	{
		displayBoard(squares, 2); fetchTarget();
	}
	if (selection == "l" || selection == "load")
	{
		loadSavedGame(); fetchTarget();
	}
	if (selection == "m" || selection == "reset")	//reset option in case no possible target for move
	{
		displayBoard(squares, 2); fetchSquare();
	}
	if (selection == "s" || selection == "save")
	{
		saveGame(); fetchTarget();
	}
	if (selection == "q" || selection == "quit") return;

	if (!goodTarget(selection)) fetchTarget();
	return;
}

bool goodTarget(std::string sq) {

	//check if sq is a square
	if (!isSquare(sq))
	{
		std::cout << "\nError: Targeted square is not a square\n";
		return false;
	}

	targeted = fetchAddress(selection); //fetchAddress(...) after isSquare to prevent crash
	MovesMade.push(targeted);									//(i.e. There's no address for non-squares)

										//check if targeted is different from selected
	if (targeted == selected)
	{
		std::cout << "\nError: Targeted square and selected square are the same?\n";
		return false;
	}

	//check if sq is empty
	if (squares[targeted].color() != ' ')
	{
		std::cout << "\nError: The target square is not empty.\n";
		return false;
	}

	if (!squares[selected].isCrowned())
	{
		if (turn == Red)
		{
			if (!oneFSAway() && !twoFSAway())
			{
				std::cout << "\nError: The target square is not within reach.\n";
				return false;
			}
			if (twoFSAway())
			{
				//if target is two squares away in front, 
				//then there has to be a capture
				if (!isCaptured())
				{
					std::cout << "\nError: The target square is two squares away, but there is no captured piece.\n";
					return false;
				}
			}
		}
		if (turn == Black)
		{
			if (!oneBSAway() && !twoBSAway())
			{
				std::cout << "\nError: The target square is not within reach.\n";
				return false;
			}
			if (twoBSAway())
			{
				//if target is two squares away in front (or behind from our view),
				//then there has to be a capture
				if (!isCaptured())
				{
					std::cout << "\nError: The target square is two squares away, but there is no piece to capture.\n";
					return false;
				}
			}
		}
	}
	if (squares[selected].isCrowned())
	{
		//crowned pieces can move forward or backward
		if (!oneFSAway() && !oneBSAway() && !twoFSAway() && !twoBSAway())
		{
			std::cout << "\nError: The target square is not within reach.\n";
			return false;
		}
		if (twoFSAway() || twoBSAway())
		{
			//if target is two squares away, 
			//then there has to be capture
			if (!isCaptured())
			{
				std::cout << "\nError: The target square is two squares away, but there is no piece to capture.\n";
				return false;
			}
		}
	}
	return true; //Success!
}

int fetchAddress(std::string sq)
{
	for (size_t i = 0; i < squares.size(); ++i)
	{
		if (sq == squares[i].square())
		{
			return i;
		}
	}
	error("Exception: used fetchAddress on non-square\n");
}

bool oneFSAway()
{
	//is the target one square away in front?
	for (size_t i = 0; i < squares[selected].getFrontAdjSqs().size(); ++i) //size() will always equal 2
	{
		if (squares[selected].getFrontAdjSqs()[i] == squares[targeted].square()) return true;
	}

	return false;
}

bool oneBSAway()
{
	//is the target one square away in back?
	for (size_t i = 0; i < squares[selected].getBackAdjSqs().size(); ++i) //size() will always equal 2
	{
		if (squares[selected].getBackAdjSqs()[i] == squares[targeted].square()) return true;
	}

	return false;
}

bool twoFSAway()
{
	//is the target two squares away and in front?
	for (size_t i = 0; i < squares[selected].getFrontJmpSqs().size(); ++i) //size() will always equal 2
	{
		if (squares[selected].getFrontJmpSqs()[i] == squares[targeted].square()) return true;
	}

	return false;
}

bool twoBSAway()
{
	//is the target two squares away and behind? (for crowned pieces)
	for (size_t i = 0; i < squares[selected].getBackJmpSqs().size(); ++i) //size() will always equal 2
	{
		if (squares[selected].getBackJmpSqs()[i] == squares[targeted].square()) return true;
	}

	return false;
}

char getLetCo_ord(std::string sq)
//returns letter coordinate of any square
{
	if (sq == "a1" || sq == "a3" || sq == "a5" || sq == "a7") return 'a';
	if (sq == "b2" || sq == "b4" || sq == "b6" || sq == "b8") return 'b';
	if (sq == "c1" || sq == "c3" || sq == "c5" || sq == "c7") return 'c';
	if (sq == "d2" || sq == "d4" || sq == "d6" || sq == "d8") return 'd';
	if (sq == "e1" || sq == "e3" || sq == "e5" || sq == "e7") return 'e';
	if (sq == "f2" || sq == "f4" || sq == "f6" || sq == "f8") return 'f';
	if (sq == "g1" || sq == "g3" || sq == "g5" || sq == "g7") return 'g';
	if (sq == "h2" || sq == "h4" || sq == "h6" || sq == "h8") return 'h';
	error("Exception: The argument of getLetCo_ord is not a square\n");
}

std::string getSqBetween(sq::Square *initSq, sq::Square *targetSq)
{
	//gets square in between by getting letCoord,
	//getting numCoord, then adding them to newSquare at end

	CapDir = getCapDirection(&initSq->square(), &targetSq->square());

	std::string newSquare = " ";
	char letCoord = ' ';
	char numCoord = ' ';

	//get the letter part of the in-between square's name
	switch (getLetCo_ord(initSq->square()))
	{
	case 'a': if (CapDir == Right) letCoord = 'b'; break;
	case 'b': if (CapDir == Right) letCoord = 'c'; break;

	case 'c': if (CapDir == Right) letCoord = 'd';
		if (CapDir == Left) letCoord = 'b'; break;

	case 'd': if (CapDir == Right) letCoord = 'e';
		if (CapDir == Left) letCoord = 'c'; break;

	case 'e': if (CapDir == Right) letCoord = 'f';
		if (CapDir == Left) letCoord = 'd'; break;

	case 'f': if (CapDir == Right) letCoord = 'g';
		if (CapDir == Left) letCoord = 'e'; break;

	case 'g': if (CapDir == Left) letCoord = 'f'; break;
	case 'h': if (CapDir == Left) letCoord = 'g'; break;

	default: error("Exception: getSqBetween(...) cannot get letter part of name.\n");
	}

	//get the number part of the square name
	if (!initSq->isCrowned())
	{
		switch (turn)
		{
		case Red:
			numCoord = targetSq->row() - 1;
			break;
		case Black:
			numCoord = targetSq->row() + 1;
			break;
		default:
			error("Exception: char turn is neither 'r' nor 'b'\n");
		}
	}
	if (initSq->isCrowned())
	{
		if (upCapture(initSq, targetSq)) numCoord = targetSq->row() - 1; //if capturing upward, treat as red piece capturing
		if (downCapture(initSq, targetSq)) numCoord = targetSq->row() + 1; //if capturing downward, treat as black piece capturing
	}

	//make newSquare out of coordinates
	newSquare = letCoord; //no addition here; that would leave a space at beginning (ex. " a1" instead of "a1")
						  //erase the space with assignment instead of addition
	newSquare += numCoord;
	return newSquare;
}

char getCapDirection(std::string *initSq, std::string *targetSq)
{
	//these two if-statements work for both red and black
	if (getLetCo_ord(*initSq) < getLetCo_ord(*targetSq))
		//ex. with b2 to d4, b is "less" than d, so direction is right
		return Right;

	if (getLetCo_ord(*initSq) > getLetCo_ord(*targetSq))
		return Left;
	error("Exception: The selected and targeted squares are on the same row\n");
}

char getRowOddEven(char row)
{
	//this is whether a number is odd or even
	//returns Odd ('O') or Even ('E'), depending on the
	//number coordinate of the square
	switch (row)
	{
	case '1': case '3': case '5': case '7':
		return Odd;
	case '2': case '4': case '6': case '8':
		return Even;
	}
	error("Exception: The row argument used in getRowOddEven is not from 1-8\n");
}

bool upCapture(sq::Square *initSq, sq::Square *targetSq) {
	if (initSq->row() < targetSq->row()) return true;
	return false;
}

bool downCapture(sq::Square *initSq, sq::Square *targetSq) {
	if (initSq->row() > targetSq->row()) return true;
	return false;
}

bool R_Capture1()
{
	//is red's first capture condition satisfied?
	//this condition is described at the top of this file as:
	// 1) start odd-numbered row, going right
	if (initialRowOddEven == Odd && CapDir == Right) return true;
	return false;
}

bool R_Capture2()
{
	//is red's second capture condition satisfied?
	//this condition is described at the top of this file as:
	// 2) start even-numbered row, going right
	if (initialRowOddEven == Even && CapDir == Right) return true;
	return false;
}

bool R_Capture3()
{
	//is red's third capture condition satisfied?
	//this condition is described at the top of this file as:
	// 3) start odd-numbered row, going left
	if (initialRowOddEven == Odd && CapDir == Left) return true;
	return false;
}

bool R_Capture4()
{
	//is red's fourth capture condition satisfied?
	//this condition is described at the top of this file as:
	// 4) start even-numbered row, going left
	if (initialRowOddEven == Even && CapDir == Left) return true;
	return false;
}

bool B_Capture1()
{
	//is black's first capture condition satisfied?
	//this condition is described at the top of this file as:
	// 1) start odd-numbered row, going left
	if (initialRowOddEven == Odd && CapDir == Left) return true;
	return false;
}

bool B_Capture2()
{
	//is black's second capture condition satisfied?
	//this condition is described at the top of this file as:
	// 2) start even-numbered row, going left
	if (initialRowOddEven == Even && CapDir == Left) return true;
	return false;
}

bool B_Capture3()
{
	//is black's third capture condition satisfied?
	//this condition is described at the top of this file as:
	// 3) start odd-numbered row, going right
	if (initialRowOddEven == Odd && CapDir == Right) return true;
	return false;
}

bool B_Capture4()
{
	//is black's fourth capture condition satisfied?
	//this condition is described at the top of this file as:
	// 4) start even-numbered row, going right
	if (initialRowOddEven == Even && CapDir == Right) return true;
	return false;
}

char oppositionColour(char color) {
	//also works on crowned colors ('R' or 'B')
	switch (color) {
	case Red: return Black;
	case Black: return Red;
	case cRed: return cBlack;
	case cBlack: return cRed;
	default: error("Exception: oppositionColour(...) used on non-color\n");
	}
}

bool isCaptured() {
	//is a capture occuring?

	//check for the capture conditions described at the top of this file
	// 1) color held by char turn

	// 2) determine oddness/evenness of initial square's row
	initialRowOddEven = getRowOddEven(squares[selected].row());

	// 3) determine capture direction
	CapDir = getCapDirection(&squares[selected].square(), &squares[targeted].square());

	//check if any capture condition is true
	//capture conditions differ for crowned vs. non-crowned
	if (!squares[selected].isCrowned())
	{
		switch (turn)
		{
		case Red:
			if (R_Capture1() && targeted - selected == rOddRightCapJmp ||
				R_Capture2() && targeted - selected == rEvenRightCapJmp ||
				R_Capture3() && targeted - selected == rOddLeftCapJmp ||
				R_Capture4() && targeted - selected == rEvenLeftCapJmp);
			else return false;

			break;
		case Black:
			if (B_Capture1() && targeted - selected == bOddLeftCapJmp ||
				B_Capture2() && targeted - selected == bEvenLeftCapJmp ||
				B_Capture3() && targeted - selected == bOddRightCapJmp ||
				B_Capture4() && targeted - selected == bEvenRightCapJmp);
			else return false;

			break;
		}
	}
	if (squares[selected].isCrowned())
	{
		if (R_Capture1() && targeted - selected == rOddRightCapJmp ||
			R_Capture2() && targeted - selected == rEvenRightCapJmp ||
			R_Capture3() && targeted - selected == rOddLeftCapJmp ||
			R_Capture4() && targeted - selected == rEvenLeftCapJmp ||
			B_Capture1() && targeted - selected == bOddLeftCapJmp ||
			B_Capture2() && targeted - selected == bEvenLeftCapJmp ||
			B_Capture3() && targeted - selected == bOddRightCapJmp ||
			B_Capture4() && targeted - selected == bEvenRightCapJmp);
		else return false;
	}

	//save vector address of SQbetween square
	SQbetween = fetchAddress(getSqBetween(&squares[selected], &squares[targeted]));
	int targ = MovesMade.top();
	MovesMade.pop();
	int Selec = MovesMade.top();
	MovesMade.pop();
	MovesMade.push(SQbetween);
	MovesMade.push(Selec);
	MovesMade.push(targ);
	if (squares[SQbetween].color() != oppositionColour(turn) && squares[SQbetween].color() != setCrown(oppositionColour(turn))) return false;

	//only do this if any of the above capture conditions is true
	//mark the in-between square as captured
	squares[SQbetween].switchCap(true);  //this square is now captured
	return true;
}

void updateGameBoard() {
	// 1) resolve piece movement:
	//to "move" a piece, change targeted square's color to
	//selected square's color, 
	squares[targeted].changeColor(squares[selected].color());

	//then make the selected square's color blank (no piece on it)
	squares[selected].changeColor(' ');

	//move the crowned "trait" from square to square
	if (squares[selected].isCrowned())
	{
		squares[selected].switchCrown(false);
		squares[targeted].switchCrown(true);
	}

	// 2) resolve captures (make captured pieces disappear):
	for (size_t i = 0; i<squares.size(); ++i)
	{
		if (squares[i].isCaptured())
		{
			squares[i].changeColor(' ');
			squares[i].switchCrown(false);
			squares[i].switchCap(false);
			wasCaptured = true;
			std::cout << "\nThe piece on " << squares[i].square() << " has been captured.\n";
		}
	}

	// 3) promote pieces to crowned
	if (isPromoted() && !squares[targeted].isCrowned())
	{
		squares[targeted].switchCrown(true);
		squares[targeted].changeColor(setCrown(squares[targeted].color()));
	}
}

bool isPromoted() {
	if (turn == Red) {
		switch (targeted) {
		case b8: case d8: case f8: case h8: return true;
		default: return false;
		}
	}
	if (turn == Black) {
		switch (targeted) {
		case a1: case c1: case e1: case g1: return true;
		default: return false;
		}
	}
}

bool redoPromotion() {
	if (turn == Red) {
		switch (redoTarget) {
		case b8: case d8: case f8: case h8: return true;
		default: return false;
		}
	}
	if (turn == Black) {
		switch (redoTarget) {
		case a1: case c1: case e1: case g1: return true;
		default: return false;
		}
	}
}

bool posCapture(sq::Square *initSq) {
	//is a capture possible?
	//(double jumps, triple jumps, etc.)
	//used after resolution of piece movement in a turn
	//if there is a possible capture, the user may make a second move to capture
	//if there is another capture, the user may again make another move, and so on

	int address = fetchAddress(initSq->square()); //store this address to check if capture is
												  //possible based on vector location
	initialRowOddEven = getRowOddEven(initSq->row());

	//int targeted still has address of landing square of piece
	if (initSq->color() == Red || initSq->isCrowned())
	{
		//check if red's first capture conditions are true and allow a capture:
		if (initialRowOddEven == Odd &&
			address + rOddRightCapJmp < squares.size() && //prevent out-of-range error
			squares[address + rOddRightCapJmp].color() == ' ' &&
			squares[address + rOddRightHafJmp].square() == getSqBetween(initSq, &squares[address + rOddRightCapJmp]))
		{
			if (squares[address + rOddRightHafJmp].color() == oppositionColour(initSq->color()) ||
				squares[address + rOddRightHafJmp].color() == setCrown(oppositionColour(initSq->color()))) return true;
		}

		//check if red's second capture conditions are true and allow a capture:
		if (initialRowOddEven == Even &&
			address + rEvenRightCapJmp < squares.size() && //prevent out-of-range error
			squares[address + rEvenRightCapJmp].color() == ' ' &&
			squares[address + rEvenRightHafJmp].square() == getSqBetween(initSq, &squares[address + rEvenRightCapJmp]))
		{
			if (squares[address + rEvenRightHafJmp].color() == oppositionColour(initSq->color()) ||
				squares[address + rEvenRightHafJmp].color() == setCrown(oppositionColour(initSq->color()))) return true;
		}

		//check if red's third capture conditions are true and allow a capture:
		if (initialRowOddEven == Odd &&
			address + rOddLeftCapJmp < squares.size() && //prevent out-of-range error
			squares[address + rOddLeftCapJmp].color() == ' ' &&
			squares[address + rOddLeftHafJmp].square() == getSqBetween(&squares[address], &squares[address + rOddLeftCapJmp]))
		{
			if (squares[address + rOddLeftHafJmp].color() == oppositionColour(initSq->color()) ||
				squares[address + rOddLeftHafJmp].color() == setCrown(oppositionColour(initSq->color()))) return true;
		}

		//check if red's fourth capture conditions are true and allow a capture:
		if (initialRowOddEven == Even &&
			address + rEvenLeftCapJmp < squares.size() && //prevent out-of-range error
			squares[address + rEvenLeftCapJmp].color() == ' ' &&
			squares[address + rEvenLeftHafJmp].square() == getSqBetween(&squares[address], &squares[address + rEvenLeftCapJmp]))
		{
			if (squares[address + rEvenLeftHafJmp].color() == oppositionColour(initSq->color()) ||
				squares[address + rEvenLeftHafJmp].color() == setCrown(oppositionColour(initSq->color()))) return true;
		}
	}
	if (initSq->color() == Black || initSq->isCrowned())
	{
		//check if black's first capture conditions are true and allow a capture:
		if (initialRowOddEven == Odd &&
			address + bOddLeftCapJmp >= LVR && //prevent out-of-range error
			squares[address + bOddLeftCapJmp].color() == ' ' &&
			squares[address + bOddLeftHafJmp].square() == getSqBetween(&squares[address], &squares[address + bOddLeftCapJmp]))
		{
			if (squares[address + bOddLeftHafJmp].color() == oppositionColour(initSq->color()) ||
				squares[address + bOddLeftHafJmp].color() == setCrown(oppositionColour(initSq->color()))) return true;
		}

		//check if black's second capture conditions are true and allow a capture:
		if (initialRowOddEven == Even &&
			address + bEvenLeftCapJmp >= LVR && //prevent out-of-range error
			squares[address + bEvenLeftCapJmp].color() == ' ' &&
			squares[address + bEvenLeftHafJmp].square() == getSqBetween(&squares[address], &squares[address + bEvenLeftCapJmp]))
		{
			if (squares[address + bEvenLeftHafJmp].color() == oppositionColour(initSq->color()) ||
				squares[address + bEvenLeftHafJmp].color() == setCrown(oppositionColour(initSq->color()))) return true;
		}

		//check if black's third capture conditions are true and allow a capture:
		if (initialRowOddEven == Odd &&
			address + bOddRightCapJmp >= LVR && //prevent out-of-range error
			squares[address + bOddRightCapJmp].color() == ' ' &&
			squares[address + bOddRightHafJmp].square() == getSqBetween(&squares[address], &squares[address + bOddRightCapJmp]))
		{
			if (squares[address + bOddRightHafJmp].color() == oppositionColour(initSq->color()) ||
				squares[address + bOddRightHafJmp].color() == setCrown(oppositionColour(initSq->color()))) return true;
		}

		//check if black's fourth capture conditions are true and allow a capture:
		if (initialRowOddEven == Even &&
			address + bEvenRightCapJmp >= LVR && //prevent out-of-range error
			squares[address + bEvenRightCapJmp].color() == ' ' &&
			squares[address + bEvenRightHafJmp].square() == getSqBetween(&squares[address], &squares[address + bEvenRightCapJmp]))
		{
			if (squares[address + bEvenRightHafJmp].color() == oppositionColour(initSq->color()) ||
				squares[address + bEvenRightHafJmp].color() == setCrown(oppositionColour(initSq->color()))) return true;
		}
	}
	return false;
}

void fetchConsecJumpTarget() {
	if (selection == "q" || selection == "quit") return;
	std::cout << "Current turn is still: " << turn << '\n';
	std::cout << "Piece to move is on " << squares[selected].square() << ".\n";
	std::cout << "Enter coordinate of target square for consecutive jump (ex. a1, f8).\n";
	std::cout << "Or enter 'n' or 'skip' to skip this consecutive jump.\n";
	// selection = get_GUI_Input();
	// std::cout << selection << '\n';
	std::cin >> selection;

	if (selection == "d" || selection == "display")
	{
		displayBoard(squares, 2); fetchConsecJumpTarget();
	}
	if (selection == "q" || selection == "quit")
	{
		saveGame();
		return;
	}
	if (selection == "n" || selection == "skip") return; //enter "s" or "skip" to skip consecutive jump
	if (!goodConsecJumpTarget(selection)) fetchConsecJumpTarget();
	return;
}

bool goodConsecJumpTarget(std::string sq) {
	//same as bool goodTarget(std::string sq), except:
	//no checking if target is empty (that's done in bool posCapture())

	//check if sq is a square
	if (!isSquare(sq))
	{
		std::cout << "\nError: Targeted square is not a square\n";
		return false;
	}

	targeted = fetchAddress(selection); //fetchAddress(...) after isSquare to prevent crash
										//(i.e. There's no address for non-squares)

										//check if targeted is different from selected
	if (targeted == selected)
	{
		std::cout << "\nError: Targeted square and selected square are the same?\n";
		return false;
	}

	if (!squares[selected].isCrowned())
	{
		if (turn == Red)
		{
			if (oneFSAway())
			{
				std::cout << "\nError: A consecutive turn must be a capture.\n" <<
					"Or enter 's' to skip the consecutive capture.\n";
				return false;
			}
			else if (twoFSAway())
			{
				//if target is two squares away in front, 
				//then there has to be a capture
				if (!isCaptured())
				{
					std::cout << "\nError: The target square is two squares away, but there is no captured piece.\n";
					return false;
				}
			}
			else
			{
				std::cout << "\nError: The target is not within reach.\n";
				return false;
			}
		}
		if (turn == Black)
		{
			if (oneBSAway())
			{
				std::cout << "\nError: A consecutive turn must be a capture.\n" <<
					"Otherwise, enter 's' to skip the consecutive capture.\n";
				return false;
			}
			else if (twoBSAway())
			{
				//if target is two squares away in front (or behind from our view),
				//then there has to be a capture
				if (!isCaptured())
				{
					std::cout << "\nError: The target square is two squares away, but there is no piece to capture.\n";
					return false;
				}
			}
			else
			{
				std::cout << "\nError: The target is not within reach.\n";
				return false;
			}
		}
	}
	if (squares[selected].isCrowned())
	{
		//crowned pieces can move forward or backward
		if (oneFSAway() || oneBSAway())
		{
			std::cout << "\nError: A consecutive turn must be a capture.\n" <<
				"Otherwise, enter 's' to skip the consecutive capture.\n";
			return false;
		}
		else if (twoFSAway() || twoBSAway())
		{
			//if target is two squares away, 
			//then there has to be capture
			if (!isCaptured())
			{
				std::cout << "\nError: The target square is two squares away, but there is no piece to capture.\n";
				return false;
			}
		}
		else
		{
			std::cout << "\nError: The target square is not within reach.\n";
			return false;
		}
	}

	return true; //Success!
}

void ShowHelpMenu() {
	std::cout << "List of key commands:\n"
		<< "\t'u' undo last move\n"
		<< "\t'r' Redo last undo\n"
		<< "\t's' Save Game\n"
		<< "\t'q' to quit\n"
		<< "\t'd' to display the game board again\n"
		<< "\t'm' to reset the current turn (to pick a different piece to move)\n";
}

void redTurn() {

	displayBoard(squares, 2);
	fetchSquare();
	if (selection == "q" || selection == "quit") 
		return;
	
	fetchTarget();
	if (selection == "q" || selection == "quit")
		return;
	
	//prints move as a test
	/*while (!MovesMade.empty())
	{
		std::string s = MovesMade.top();
		std::cout << s << " ";
		MovesMade.pop();
	}*/
	updateGameBoard();
	while (wasCaptured && posCapture(&squares[targeted]))
	{
		selected = targeted;
		displayBoard(squares, 2);
		fetchConsecJumpTarget();
		if (selection == "q" || selection == "quit") return;
		if (selection == "n" || selection == "skip") return; //do this before updateGameBoard()
															 //otherwise updateGameBoard removes squares[selected]
		updateGameBoard();
	}

}

void blackTurn() {
	displayBoard(squares, 2);
	fetchSquare();
	if (selection == "q" || selection == "quit")
		return;
	
	fetchTarget();
	if (selection == "q" || selection == "quit")
	
		return;
	
	//prints move as a test
	/*while(!MovesMade.empty())
	{
		std::string s = MovesMade.top();
		std::cout << s << " ";
		MovesMade.pop();
	}*/
	updateGameBoard();
	while (wasCaptured && posCapture(&squares[targeted]))
	{
		selected = targeted;
		displayBoard(squares, 2);
		fetchConsecJumpTarget();
		if (selection == "q" || selection == "quit") return;
		if (selection == "n" || selection == "skip") return; //do this before updateGameBoard()
															 //otherwise updateGameBoard removes squares[selected]
		updateGameBoard();
	}
}






void AIMakeMove()
{
	AIMove();
	//updateGameBoard();
}

void AIMove()
{
	int possSelect = 0;
	int possTarget = 0;
	int lPossMove = 32;
	sel = 0;
	AISelectMove = 0;
	possAISelect.clear();
	possAITarget.clear();
	//finds black pieces on the baord
	while (lPossMove > possSelect)
	{
		int n = possSelect;
		if (squares[n].color() == Black || squares[n].color() == cBlack)
		{
			//test to see if the value n corresponds witht the correct squares
			//std::cout << n << " ";
			possAISelect.push_back(n);
		}
		possSelect++;
	}
	std::cout << "\n";
	while (lPossMove > possTarget)
	{
		int isEmpty = possTarget;
		if (squares[isEmpty].color() == ' ')
		{
			//test to see if the value n corresponds witht the correct squares
			//std::cout << isEmpty << " ";
			possAITarget.push_back(isEmpty);
		}
		possTarget++;
	}
	minselectVal = *std::_Min_value(possAISelect.begin(), possAISelect.end());
	sel = minselectVal;
	AISelectMove = sel;

	//test to see the output of the min_value sorting of the vector
	//std::cout << "\n" << minselectVal;

	for (int v = AISelectMove; v < maxSelectVal; ++v)
	{
		if (squares[AISelectMove].color() == Black || squares[AISelectMove].color() == cBlack)
		{
			if (possibleAIMove())
			{
				//std::cout << v  << AISelectMove << "\n";
				return;
			}
			else if (!possibleAIMove())
			{
				v++;
				//std::cout << v << "\n";
				// set new sel val
				AISelectMove = v;
				//std::cout << AISelectMove << "\n";
				
			}
		}
		else if (squares[AISelectMove].color() != Black || !squares[AISelectMove].color() != cBlack)
		{
			v++;
			AISelectMove = v;
			//std::cout << AISelectMove << "\n";
			
		}
	}
}

bool possibleAIMove()
{
	int t1 = AISelectMove - 4;
	int t2 = AISelectMove - 5;
	int t3 = AISelectMove - 7;
	int t4 = AISelectMove - 9;
	int t5 = AISelectMove + 4;
	int t6 = AISelectMove + 5;
	int t7 = AISelectMove + 7;
	int t8 = AISelectMove + 9;
	//AI moves for non crowned piece
		if (!squares[sel].isCrowned())
		{
			//std::cout << x << "\n";
			
			// If no capture then single move is performed
			if (std::find(possAITarget.begin(), possAITarget.end(), t1) != possAITarget.end() && squares[t1].color() == ' ')
			{
				AITargetMove = t1;
				squares[AITargetMove].changeColor(squares[AISelectMove].color());
				squares[AISelectMove].changeColor(' ');
				std::cout << AISelectMove << " is moved to " << AITargetMove << "\n";
				MovesMade.push(AISelectMove);
				MovesMade.push(AITargetMove);
				AISelectMove = 0;
				return true;
			}
			else if (std::find(possAITarget.begin(), possAITarget.end(), t2) != possAITarget.end() && squares[t2].color() == ' ')
			{
				AITargetMove = t2;
				squares[AITargetMove].changeColor(squares[AISelectMove].color());
				squares[AISelectMove].changeColor(' ');
				std::cout << AISelectMove << " can move to " << AITargetMove << "\n";
				MovesMade.push(AISelectMove);
				MovesMade.push(AITargetMove);
				AISelectMove = 0;
				return true;
			}
			//checks if AI can perform a capture move
			if (std::find(possAITarget.begin(), possAITarget.end(), t3) != possAITarget.end() && squares[t3].color() == ' ')
			{
				AITargetMove = t3;
				squares[AITargetMove].changeColor(squares[AISelectMove].color());
				squares[AISelectMove].changeColor(' ');
				//handles the capture move
				if (AISelectMove == 25 || AISelectMove == 26 || AISelectMove == 24 || AISelectMove == 17 || AISelectMove == 18 || AISelectMove == 16 ||
					AISelectMove == 9 || AISelectMove == 10 || AISelectMove == 8)
				{
					int cap = AISelectMove - 4;
					MovesMade.push(cap);
					squares[cap].changeColor(' ');
				}
				if (AISelectMove == 29 || AISelectMove == 30 || AISelectMove == 28 || AISelectMove == 21 || AISelectMove == 22 || AISelectMove == 20 ||
					AISelectMove == 13 || AISelectMove == 14 || AISelectMove == 12)
				{
					int cap = AISelectMove - 3;
					MovesMade.push(cap);
					squares[cap].changeColor(' ');
				}
				std::cout << AISelectMove << " can move to " << AITargetMove << "\n";
				MovesMade.push(AISelectMove);
				MovesMade.push(AITargetMove);
				AISelectMove = 0;
				return true;
			}
			else if (std::find(possAITarget.begin(), possAITarget.end(), t4) != possAITarget.end() && squares[t4].color() == ' ')
			{
				AITargetMove = t4;
				squares[AITargetMove].changeColor(squares[AISelectMove].color());
				squares[AISelectMove].changeColor(' ');
				if (AISelectMove == 25 || AISelectMove == 26 || AISelectMove == 27 || AISelectMove == 17 || AISelectMove == 18 || AISelectMove == 19 ||
					AISelectMove == 9 || AISelectMove == 10 || AISelectMove == 11)
				{
					int cap = AISelectMove - 5;
					MovesMade.push(cap);
					squares[cap].changeColor(' ');
				}
				if (AISelectMove == 29 || AISelectMove == 30 || AISelectMove == 31 || AISelectMove == 21 || AISelectMove == 22 || AISelectMove == 23 ||
					AISelectMove == 13 || AISelectMove == 14 || AISelectMove == 15)
				{
					int cap = AISelectMove - 4;
					MovesMade.push(cap);
					squares[cap].changeColor(' ');
				}
				std::cout << AISelectMove << " can move to " << AITargetMove << "\n";
				MovesMade.push(AISelectMove);
				MovesMade.push(AITargetMove);
				AISelectMove = 0;
				return true;
			}
			return false;
		}
		//AI for Crowned Piece
		if (squares[sel].isCrowned())
		{
			//std::cout << x << "\n";
			if (std::find(possAITarget.begin(), possAITarget.end(), t1) != possAITarget.end() && squares[t1].color() == ' ')
			{
				AITargetMove = t1;
				squares[AITargetMove].changeColor(squares[AISelectMove].color());
				squares[AISelectMove].changeColor(' ');
				std::cout << AISelectMove << " can move to " << AITargetMove << "\n";
				MovesMade.push(AISelectMove);
				MovesMade.push(AITargetMove);
				AISelectMove = 0;
				return true;
			}
			else if (std::find(possAITarget.begin(), possAITarget.end(), t2) != possAITarget.end() && squares[t2].color() == ' ')
			{
				AITargetMove = t2;
				squares[AITargetMove].changeColor(squares[AISelectMove].color());
				squares[AISelectMove].changeColor(' ');
				std::cout << AISelectMove << " can move to " << AITargetMove << "\n";
				MovesMade.push(AISelectMove);
				MovesMade.push(AITargetMove);
				AISelectMove = 0;
				return true;
			}
			if (std::find(possAITarget.begin(), possAITarget.end(), t3) != possAITarget.end() && squares[t3].color() == ' ')
			{
				AITargetMove = t3;
				squares[AITargetMove].changeColor(squares[AISelectMove].color());
				squares[AISelectMove].changeColor(' ');
				//handles the capture move
				if (AISelectMove == 25 || AISelectMove == 26 || AISelectMove == 24 || AISelectMove == 17 || AISelectMove == 18 || AISelectMove == 16 ||
					AISelectMove == 9 || AISelectMove == 10 || AISelectMove == 8)
				{
					int cap = AISelectMove - 4;
					MovesMade.push(cap);
					squares[cap].changeColor(' ');
				}
				if (AISelectMove == 29 || AISelectMove == 30 || AISelectMove == 28 || AISelectMove == 21 || AISelectMove == 22 || AISelectMove == 20 ||
					AISelectMove == 13 || AISelectMove == 14 || AISelectMove == 12)
				{
					int cap = AISelectMove - 3;
					MovesMade.push(cap);
					squares[cap].changeColor(' ');
				}
				std::cout << AISelectMove << " can move to " << AITargetMove << "\n";
				MovesMade.push(AISelectMove);
				MovesMade.push(AITargetMove);
				AISelectMove = 0;
				return true;
			}
			else if (std::find(possAITarget.begin(), possAITarget.end(), t4) != possAITarget.end() && squares[t4].color() == ' ')
			{
				AITargetMove = t4;
				squares[AITargetMove].changeColor(squares[AISelectMove].color());
				squares[AISelectMove].changeColor(' ');
				if (AISelectMove == 25 || AISelectMove == 26 || AISelectMove == 27 || AISelectMove == 17 || AISelectMove == 18 || AISelectMove == 19 ||
					AISelectMove == 9 || AISelectMove == 10 || AISelectMove == 11)
				{
					int cap = AISelectMove - 5;
					MovesMade.push(cap);
					squares[cap].changeColor(' ');
				}
				if (AISelectMove == 29 || AISelectMove == 30 || AISelectMove == 31 || AISelectMove == 21 || AISelectMove == 22 || AISelectMove == 23 ||
					AISelectMove == 13 || AISelectMove == 14 || AISelectMove == 15)
				{
					int cap = AISelectMove - 4;
					MovesMade.push(cap);
					squares[cap].changeColor(' ');
				}
				std::cout << AISelectMove << " can move to " << AITargetMove << "\n";
				MovesMade.push(AISelectMove);
				MovesMade.push(AITargetMove);
				AISelectMove = 0;
				return true;
			}
			if (std::find(possAITarget.begin(), possAITarget.end(), t5) != possAITarget.end() && squares[t5].color() == ' ')
			{
				AITargetMove = t5;
				squares[AITargetMove].changeColor(squares[AISelectMove].color());
				squares[AISelectMove].changeColor(' ');
				std::cout << AISelectMove << " can move to " << AITargetMove << "\n";
				MovesMade.push(AISelectMove);
				MovesMade.push(AITargetMove);
				AISelectMove = 0;
				return true;
			}
			else if (std::find(possAITarget.begin(), possAITarget.end(), t6) != possAITarget.end() && squares[t6].color() == ' ')
			{
				AITargetMove = t6;
				squares[AITargetMove].changeColor(squares[AISelectMove].color());
				squares[AISelectMove].changeColor(' ');
				std::cout << AISelectMove << " can move to " << AITargetMove << "\n";
				MovesMade.push(AISelectMove);
				MovesMade.push(AITargetMove);
				AISelectMove = 0;
				return true;
			}
			if (std::find(possAITarget.begin(), possAITarget.end(), t7) != possAITarget.end() && squares[t7].color() == ' ')
			{
				AITargetMove = t7;
				squares[AITargetMove].changeColor(squares[AISelectMove].color());
				squares[AISelectMove].changeColor(' ');
				//handles the capture move
				if (AISelectMove == 5 || AISelectMove == 6 || AISelectMove == 7 || AISelectMove == 13 || AISelectMove == 14 || AISelectMove == 15 ||
					AISelectMove == 21 || AISelectMove == 22 || AISelectMove == 23)
				{
					int cap = AISelectMove + 4;
					MovesMade.push(cap);
					squares[cap].changeColor(' ');
				}
				if (AISelectMove == 1 || AISelectMove == 2 || AISelectMove == 3 || AISelectMove == 9 || AISelectMove == 10 || AISelectMove == 11 ||
					AISelectMove == 17 || AISelectMove == 18 || AISelectMove == 19)
				{
					int cap = AISelectMove + 3;
					MovesMade.push(cap);
					squares[cap].changeColor(' ');
				}
				std::cout << AISelectMove << " can move to " << AITargetMove << "\n";
				MovesMade.push(AISelectMove);
				MovesMade.push(AITargetMove);
				AISelectMove = 0;
				return true;
			}
			else if (std::find(possAITarget.begin(), possAITarget.end(), t8) != possAITarget.end() && squares[t8].color() == ' ')
			{
				AITargetMove = t8;
				squares[AITargetMove].changeColor(squares[AISelectMove].color());
				squares[AISelectMove].changeColor(' ');
				if (AISelectMove == 4 || AISelectMove == 5 || AISelectMove == 6 || AISelectMove == 12 || AISelectMove == 13 || AISelectMove == 14 ||
					AISelectMove == 20 || AISelectMove == 21 || AISelectMove == 22)
				{
					int cap = AISelectMove + 5;
					MovesMade.push(cap);
					squares[cap].changeColor(' ');
				}
				if (AISelectMove == 0 || AISelectMove == 1 || AISelectMove == 2 || AISelectMove == 8 || AISelectMove == 9 || AISelectMove == 10 ||
					AISelectMove == 16 || AISelectMove == 17 || AISelectMove == 18)
				{
					int cap = AISelectMove + 4;
					MovesMade.push(cap);
					squares[cap].changeColor(' ');
				}
				std::cout << AISelectMove << " can move to " << AITargetMove << "\n";
				MovesMade.push(AISelectMove);
				MovesMade.push(AITargetMove);
				AISelectMove = 0;
				return true;
			}
			return false;
		}
}

void undoLastMove()
{
	//
	//define the selected piece to move back 
	undoSelect = MovesMade.top();
	MovesMade.pop();
	// define the target square for the undo move
	undoTarget = MovesMade.top();
	//push values back to stack for redo move
	
	/*following used to test undo target selection
	std::cout << undoSelec << " " << undoTarg*/

	MovesMade.pop();
	int sum = undoTarget - undoSelect;
	//test to make sure sum gave correct result
	std::cout << sum << "\n";
	if (sum == -4 || sum == 4 || sum == 5 || sum == -5 || sum == 3 || sum == -3)
	{
		/* 1) resolve piece movement using last two entries pushed to the stack:
		to "undo" move first value popped from stack is the selection and second popped off is the target.
		sas the move was legal no checks needed and pice can be moved back.*/
		squares[undoTarget].changeColor(squares[undoSelect].color());
		if (squares[undoSelect].isCrowned())
		{
			squares[undoTarget].switchCrown(false);
			squares[undoTarget].changeColor(setCrown(squares[undoTarget].color()));
		}

		//then make the selected square's color blank
		squares[undoSelect].changeColor(' ');
	}
	if (sum == 9 || sum == -9 || sum == 7 || sum == -7)
	{
		if (turn == Red)
		{
			int undoCapture = MovesMade.top();
			MovesMade.pop();
			RedoMoves.push(undoCapture);
			squares[undoTarget].changeColor(squares[undoSelect].color());
			if (squares[undoSelect].isCrowned())
			{
				squares[undoTarget].switchCrown(false);
				squares[undoTarget].changeColor(setCrown(setCrown(oppositionColour(turn))));
			}
			//then make the selected square's color blank
			squares[undoSelect].changeColor(' ');
			squares[undoCapture].changeColor(turn);
		}
		else if (turn == Black)
		{
			int undoCapture = MovesMade.top();
			MovesMade.pop();
			RedoMoves.push(undoCapture);
			squares[undoTarget].changeColor(squares[undoSelect].color());
			if (squares[undoSelect].isCrowned())
			{
				squares[undoTarget].switchCrown(false);
				squares[undoTarget].changeColor(setCrown(setCrown(oppositionColour(turn))));
			}
			//then make the selected square's color blank
			squares[undoSelect].changeColor(' ');
			squares[undoCapture].changeColor(turn);
		}

	}

	RedoMoves.push(undoSelect);
	RedoMoves.push(undoTarget);
	//reverses players turn so that it is the previous players turn
	displayBoard(squares, 2);
	turn = oppositionColour(turn);
	std::cout << "Would you like to undo another move? y/n\n";
	std::cin >> selection;
	if (selection == "y")
	{
		undoLastMove();
	}
	if (selection == "n")
	{
		displayBoard(squares, 2);
	}
}

void redoMove()
{
	if (!RedoMoves.empty())
	{
		redoSelect = RedoMoves.top();
		RedoMoves.pop();
		redoTarget = RedoMoves.top();
		RedoMoves.pop();
		std::cout << redoTarget << " " << redoSelect;
		int sum = redoTarget - redoSelect;
		//test to make sure sum gave correct result
		std::cout << sum << "\n";
		/* 1) resolve piece movement using last two entries pushed to the stack:
		to "undo" move first value popped from stack is the selection and second popped off is the target.
		sas the move was legal no checks needed and pice can be moved back.*/
		squares[redoTarget].changeColor(squares[redoSelect].color());

		//then make the selected square's color blank
		squares[redoSelect].changeColor(' ');


		if (sum == 9 || sum == -9 || sum == 7 || sum == -7)
		{
			if (turn == Red)
			{
				redoCapture = RedoMoves.top();
				RedoMoves.pop();
				MovesMade.push(redoCapture);

				if (redoPromotion() && !squares[redoTarget].isCrowned())
				{
					squares[redoTarget].switchCrown(true);
					squares[redoTarget].changeColor(setCrown(squares[undoTarget].color() == cRed));
				}
				//then make the selected square's color blank
				squares[redoSelect].changeColor(' ');
				squares[redoCapture].changeColor(' ');

			}
			else if (turn == Black)
			{
				redoCapture = RedoMoves.top();
				RedoMoves.pop();
				MovesMade.push(redoCapture);
				if (redoPromotion() && !squares[redoTarget].isCrowned())
				{
					squares[redoTarget].switchCrown(true);
					squares[redoTarget].changeColor(setCrown(squares[undoTarget].color() == cBlack));
				}
				//then make the selected square's color blank
				squares[redoSelect].changeColor(' ');
				squares[redoCapture].changeColor(' ');
			}
		}
		MovesMade.push(redoSelect);
		MovesMade.push(redoTarget);
		displayBoard(squares, 2);
		turn = oppositionColour(turn);
	}
	if (RedoMoves.empty())
	{
		std::cout << "no moves to be redone\n";
		displayBoard(squares, 2);
		turn = turn;
	}
}

bool isGameOver() {
	bool risGameOver = true;
	bool bisGameOver = true;

	if (loser == Both) return true; //in case of draw it is game over if either side has no pieces remaining
	for (size_t i = 0; i < squares.size(); ++i)
	{
		//if there is a piece of a certain color,
		//then that side has not lost
		if (squares[i].color() == Red || squares[i].color() == cRed) risGameOver = false;
		if (squares[i].color() == Black || squares[i].color() == cBlack) bisGameOver = false;
	}
	if (risGameOver && bisGameOver) error("Exception: All of the pieces have disappeared\n");
	if (risGameOver) { loser = Red; return true; }
	if (bisGameOver) { loser = Black; return true; }
	return false;
}

void CheckersGame() {
	std::cout << "Which game would you like to play? \n";
	std::cout << "Enter 1 to play against a friend. \n";
	std::cout << "Enter 2 to play against the computer. \n";
	std::cout << "Enter 3 to auto play the last saved game. \n";
	std::cin >> GameType;
	if (GameType == "1")
	{
		PrepGame();
		bool quit = false;
		std::cout << "\nBefore you start heres a ";
		ShowHelpMenu();
		while (!isGameOver() && !quit)
		{
			if (cantMove())
			{
				//if there is no possible move for the turn player (which can happen in checkers),
				//then the game is a draw
				loser = Both; //this will cause isGameOver() to return true
				saveGame();
			}
			else if (turn == Red)
			{
				redTurn();
				if (selection == "q" || selection == "quit") quit = true;
				turn = oppositionColour(turn);
			}
			else if (turn == Black)
			{
				blackTurn();
				if (selection == "q" || selection == "quit") quit = true;
				turn = oppositionColour(turn);
			}
			wasCaptured = false; //prepare for next turn
		}
		if (isGameOver() && !quit)
		{
			whoLost();
			saveGame();
		}
		if (ReMatch()) CheckersGame();
	}
	if (GameType == "2")
	{
		PrepGame();
		bool quit = false;
		std::cout << "\nBefore you start heres a ";
		ShowHelpMenu();
		while (!isGameOver() && !quit)
		{
			if (cantMove())
			{
				//if there is no possible move for the turn player (which can happen in checkers),
				//then the game is a draw
				loser = Both; //this will cause isGameOver() to return true
			}
			else if (turn == Red)
			{
				redTurn();
				if (selection == "q" || selection == "quit") quit = true;
				turn = oppositionColour(turn);
			}
			else if (turn == Black)
			{
				AIMakeMove();
				if (selection == "q" || selection == "quit") quit = true;
				turn = oppositionColour(turn);
			}
			wasCaptured = false; //prepare for next turn
		}
		if (isGameOver() && !quit)
		{
			whoLost();
		}
		if (ReMatch()) CheckersGame();
	}
	if (GameType == "3")
	{
		PrepGame();
		bool quit = false;
		loadSavedGame();
		CheckersGame();
	}
}



void whoLost() {
	if (loser == Red) redLoss();
	if (loser == Black) blackLoss();
	if (loser == Both) GameDraw();
}

void redLoss() {
	//tells the user that red has lost
	displayBoard(squares, 2);
	std::cout << "Game over! Red loses. Black wins.\n";
}

void blackLoss() {
	//tells the user that black has lost
	displayBoard(squares, 2);
	std::cout << "Game over! Black loses. Red wins.\n";
}

void GameDraw() {
	//tells the user that the game is a draw
	displayBoard(squares, 2);
	std::cout << "Game over! The game is a draw.\n";
	if (turn == Red) std::cout << "Red cannot make a move.\n";
	if (turn == Black) std::cout << "Black cannot make a move.\n";
}

void saveGame()
{
	std::ofstream savefile;
	savefile.open("saveGame.txt");
	while (!MovesMade.empty())
	{
		stringSave = std::to_string(MovesMade.top());
		MovesMade.pop();
		// writes the string to the file as it pops off the top value of the stack `MovesMade`
		//std::cout << stringSave << " ";
		savefile << stringSave << "\n";
		
	}
	std::cout << "Game Saved.\n";
	savefile.close();
}

void loadSavedGame()
{
	char contents[100];
	std::ifstream ReadSavedGame("savegame.txt");
	int a;
	while (ReadSavedGame >> a)
	{
		//std::cout << a << "\n";
		RedoMoves.push(a);
	}
	/*while (!RedoMoves.empty())
	{
		int top = RedoMoves.top();
		std::cout << top << "\n";
		RedoMoves.pop();
	}*/
	while (!RedoMoves.empty())
	{
		redoMove();
	}
	ReadSavedGame.close();
}

bool ReMatch() {
	//does the user want another game?
	std::cout << "\nWould you like to play another game?\n";
	while (selection != "yes" && selection != "y" && selection != "no" && selection != "n")
	{
		std::cout << "Type 'y' or 'n':\n"; std::cin >> selection;
	}
	if (selection == "yes" || selection == "y") return true;
	return false;
}

void Exit() {
	//handles Exit
	std::cout << "Enter a character to Exit:\n";
	char c;
	std::cin >> c;
}