//this file sets variables for the game rules to make sure that the moves are legal

#ifndef Functions_H
#define Functions_H

#include "CheckersBoard.h"
#include "Square.h"
#include "CheckersVars.h"
#include "Constants.h"

using namespace checkers;
#include <string>
#include <iostream>

void PrepGame();

bool isSquare(std::string square);
char setCrown(char colour); // sets crowned ("R" "B") or uncrowned ("r" "b") vlaue of piece
bool cantMove();
bool possibleMove(sq::Square *initSq);
void fetchSquare();
bool goodSquare(std::string sq);
void fetchTarget();
bool goodTarget(std::string sq);
//void fetchUndoTarget();
//bool goodUndoTarget(std::string sq);
bool oneFSAway();
bool oneBSAway();
bool twoFSAway();
bool twoBSAway();
int fetchAddress(std::string sq);
char getLetCo_ord(std::string sq1);
std::string getSqBetween(sq::Square *initSq, sq::Square *targetSq);
char getCapDirection(std::string *initSq, std::string *targetSq);
char getRowOddEven(char row);

//functions for crowned pieces for function sq::Square getSqBetween
bool upCapture(sq::Square *initSq, sq::Square *targetSq);
bool downCapture(sq::Square *initSq, sq::Square *targetSq);

// AI functions
void AIMove();
bool possibleAIMove();
void AIMakeMove();
void undoLastMoveAI();

//func for non-crowned pieces
bool R_Capture1();
bool R_Capture2();
bool R_Capture3();
bool R_Capture4();
bool B_Capture1();
bool B_Capture2();
bool B_Capture3();
bool B_Capture4();
char oppositionColour(char colour);
bool isCaptured();
void updateGameBoard();
bool isPromoted();
bool redoPromotion();
bool posCapture(sq::Square *initSq);
void fetchConsecJumpTarget(); // used if a double jump is performed
bool goodConsecJumpTarget(std::string sq);
void ShowHelpMenu();
void redTurn();
void blackTurn();
void undoLastMove();
void redoMove();
bool isGameOver();
void redLoss();
void blackLoss();
void GameDraw();
void saveGame();
void loadSavedGame();
void CheckersGame();
void whoLost();
bool ReMatch();
void Exit();

#endif
