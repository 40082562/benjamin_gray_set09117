#include <iostream>
#include "stdafx.h"
#include "Functions.h"
#include "CheckersVars.h"

int _tmain()
try
{
	CheckersGame();

	return 0;
}
catch (std::string message)
{
	std::cerr << message << '\n';
	Exit();
	return 1;
}
catch (...)
{
	std::cerr << "Unknown exception\n";
	Exit();
	return 2;
}