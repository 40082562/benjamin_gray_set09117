#ifndef CheckersVars_h
#define CheckersVars_h

#include <string>
#include <vector>
#include <iostream>
#include <exception>
#include "square.h"
#include <stack>

namespace checkers
{
	extern std::vector<sq::Square> squares;
	extern std::vector<int> possAISelect;
	extern std::vector<int> possAITarget;
	extern std::string selection;
	extern std::string GameType;
	extern std::string undoSQSelection;
	extern std::string undoTargSelection;
	extern std::stack<int> MovesMade;
	extern std::stack<int> RedoMoves;
	extern std::stack<int> AIOrderMoves;
	extern std::string startMoveValue;
	extern std::string endMoveValue;
	extern std::string stringSave;
	extern char turn;
	extern char CapDir;
	extern char initialRowOddEven;
	extern bool wasCaptured;
	extern char loser;

	extern int selected;
	extern int targeted;
	extern int SQbetween;
	extern int redoSelect;
	extern int redoTarget;
	extern int redoCapture;
	extern int undoSelect;
	extern int undoTarget;

	// AI vals
	extern int minselectVal;
	extern int maxSelectVal;
	extern int minTargetVal;
	extern int maxTargetVal;
	extern int sel;
	extern int AISelectMove;
	extern int AITargetMove;
}

#endif