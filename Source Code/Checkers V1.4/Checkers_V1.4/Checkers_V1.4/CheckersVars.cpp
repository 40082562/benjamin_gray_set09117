#include "stdafx.h"
#include "CheckersVars.h"

std::vector<sq::Square> checkers::squares(32, sq::Square(' ', "a1", '1'));
std::vector<int> checkers::possAISelect;
std::vector<int> checkers::possAITarget;
std::string checkers::selection = " ";
std::string checkers::GameType = " ";
std::string checkers::undoSQSelection = " ";
std::string checkers::undoTargSelection = " ";
std::string checkers::startMoveValue = " ";
std::string checkers::endMoveValue = " ";
std::stack<int> checkers::MovesMade;
std::stack<int> checkers::RedoMoves;
std::stack<int> checkers::AIOrderMoves;
std::string checkers::stringSave = " ";
char checkers::turn = ' ';
char checkers::CapDir = ' ';
char checkers::initialRowOddEven = ' ';
bool checkers::wasCaptured = false;
char checkers::loser = ' ';

int checkers::selected = 0;
int checkers::targeted = 0;
int checkers::SQbetween = 0;
int checkers::redoSelect = 0;
int checkers::redoTarget = 0;
int checkers::redoCapture = 0;
int checkers::undoSelect = 0;
int checkers::undoTarget = 0;

//AI vals
int checkers::minselectVal = 0;
int checkers::maxSelectVal = 0;
int checkers::minTargetVal = 0;
int checkers::maxTargetVal = 0;
int checkers::sel = 0;
int checkers::AISelectMove = 0;
int checkers::AITargetMove = 0;