#ifndef CheckersBoard_h
#define CheckersBoard_h

#include <iostream>
#include <vector>
#include <string>
#include "Square.h"

void displayBoard(const std::vector<sq::Square> &sqVect, int boardWidth);
void narrowBoard(const std::vector<sq::Square> &sqVect);
void regularBoard(const std::vector<sq::Square> &sqVect);

#endif