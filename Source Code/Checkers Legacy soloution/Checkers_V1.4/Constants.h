#ifndef Constants_h
#define Constants_h

namespace constants
{
	const char Red = 'r'; // regular red piece
	const char Black = 'b'; // Regular BlackPiece
	const char cRed = 'R'; // Red crowned
	const char cBlack = 'B'; // Black crowned
	const char Both = 'X'; // used if a tie occurs
	const char Left = 'L';
	const char Right = 'R';
	const char Odd = 'O';
	const char Even = 'E';
	const int LVR = 0; // LVR = Low Vecto Range, lowest range for the vectors

					   // squares at the end of the board are defined with the following addresses
	const int b8 = 28;
	const int d8 = 29;
	const int f8 = 30;
	const int h8 = 31;
	const int a1 = 0;
	const int c1 = 1;
	const int e1 = 2;
	const int g1 = 3;

	const int rOddRightCapJmp = 9; // red's first cap cond
	const int rOddRightHafJmp = 4;

	const int rEvenRightCapJmp = 9; //red's second cap cond
	const int rEvenRightHafJmp = 5;

	const int rOddLeftCapJmp = 7; //red's third capture condition
	const int rOddLeftHafJmp = 3;

	const int rEvenLeftCapJmp = 7; //red's fourth capture condition
	const int rEvenLeftHafJmp = 4;

	const int bOddLeftCapJmp = -9; //black's first capture condition
	const int bOddLeftHafJmp = -5;

	const int bEvenLeftCapJmp = -9; //black's second capture condition
	const int bEvenLeftHafJmp = -4;

	const int bOddRightCapJmp = -7; //black's third capture condition
	const int bOddRightHafJmp = -4;

	const int bEvenRightCapJmp = -7; //black's fourth capture condition
	const int bEvenRightHafJmp = -3;
}

using namespace constants;

#endif