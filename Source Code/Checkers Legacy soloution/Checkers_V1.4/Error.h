#ifndef Error_h
#define Error_h

#include <string>

void error(std::string message);

#endif